package com.mety.rest;

import com.mety.rest.apis.User;

import java.util.List;

import retrofit.http.GET;

/**
 * Created by aboieriu on 3/6/15.
 */
public interface UserService {
    public static final String BASE_URL = "http://10.0.0.96:8080";

        @GET("/user/Miki")
        public List<User> getUser();
}
