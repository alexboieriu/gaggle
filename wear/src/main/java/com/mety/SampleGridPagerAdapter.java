package com.mety;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;

/**
 * Created by aboieriu on 6/6/2015.
 */
public class SampleGridPagerAdapter extends FragmentGridPagerAdapter {

    private final Context mContext;

    private Page[][] PAGES = new Page[][] {
            {new Page(R.string.adi_title, R.string.adi_desc), new Page(R.string.adi_title, R.string.adi_desc)},
            {new Page(R.string.miki_title, R.string.miki_desc), new Page(R.string.miki_title, R.string.miki_desc)},
            {new Page(R.string.lau_title, R.string.lau_desc), new Page(R.string.lau_title, R.string.lau_desc)}};

    static final int[] BG_IMAGES = new int[] {R.drawable.adi, R.drawable.lau, R.drawable.miki};

    public SampleGridPagerAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        mContext = ctx;
    }

    private class Page {
        public Page(int textRes, int iconRes) {
            this.textRes = textRes;
            this.cardGravity = 5;
            this.expansionEnabled = true;
            this.expansionFactor = 2;
        }

        // static resources
        int titleRes;
        int textRes;
        int iconRes;
        int cardGravity;
        boolean expansionEnabled;
        int expansionDirection;
        int expansionFactor;
    }





    @Override
    public Fragment getFragment(int row, int col) {
        Page page = PAGES[row][col];
        String title =  page.titleRes != 0 ? mContext.getString(page.titleRes) : null;
        String text = page.textRes != 0 ? mContext.getString(page.textRes) : null;
        CardFragment fragment = CardFragment.create(title, text, page.iconRes);

        // Advanced settings (card gravity, card expansion/scrolling)
        fragment.setCardGravity(page.cardGravity);
        fragment.setExpansionEnabled(page.expansionEnabled);
        fragment.setExpansionDirection(page.expansionDirection);
        fragment.setExpansionFactor(page.expansionFactor);

        return fragment;
    }

    @Override
    public int getRowCount() {
        return PAGES.length;
    }

    @Override
    public int getColumnCount(int rowNum) {
        return PAGES[rowNum].length;
    }

}
